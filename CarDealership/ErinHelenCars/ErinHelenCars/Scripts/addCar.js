﻿$().ready(function () {
    //on click show modal
    $('#showModalAddCar').click(function () {
        $('#addCarModal').modal('show');
    });

    $('#btnSaveCar').click(function () {
        var car = {}; //new object

        //get values from all inputs
        car.Make = $('#make').val();
        car.Model = $('#model').val();
        car.Year = $('#year').val();
        car.Mileage = $('#mileage').val();
        car.AdTitle = $('#adTitle').val();
        car.Description = $('#description').val();
        car.Price = $('#price').val();
        car.URLofPicture = $('#pictureURL').val();
        if ($('#isAvailable').is(":checked")) {
            car.isAvailable = true;
        }
        else {
            car.isAvailable = false;
        }

        //post values to the web API
        $.post(uri, car)
        .done(function () {
            loadCars();
            $('#addCarModal').modal('hide');
        })
        .fail(function (jqXhr, status, err) {
            alert(status + ' - ' + err);
        });
    });
});