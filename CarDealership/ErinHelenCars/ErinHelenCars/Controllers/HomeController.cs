﻿using ErinHelenCars.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ErinHelenCars.Controllers
{
    public class HomeController : Controller
    {
        private HEEZcarsEntities db = new HEEZcarsEntities();

        public ActionResult Index()
        {
            ViewData["vehicleID"] = new SelectList(db.whipzs, "vehicleID", "make");
            return View();
        }

        public ActionResult ViewModalRequestForm()
        {
            return PartialView("_modalRequestForm");
        }

        [HttpPost]
        public ActionResult ModalRequestForm()
        {
            return RedirectToAction("Index", "requestsForInfoes");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}