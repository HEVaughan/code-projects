﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ErinHelenCars.Models;

namespace ErinHelenCars.Controllers
{
    public class RequestAPIController : ApiController
    {
        private HEEZcarsEntities db = new HEEZcarsEntities();
        // GET api/<controller>
        public IQueryable<requestsForInfo> Getrequests()
        {
            return db.requestsForInfoes;
        }

        [ResponseType(typeof(requestsForInfo))]
        public IHttpActionResult Getrequests(int id)
        {
            requestsForInfo requests = db.requestsForInfoes.Find(id);
            if (requests == null)
            {
                return NotFound();
            }

            return Ok(requests);
        }

        // PUT: api/RequestAPI/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putrequests(int id, requestsForInfo requests)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != requests.requestID)
            {
                return BadRequest();
            }

            db.Entry(requests).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!requestsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/RequestAPI
        [ResponseType(typeof(requestsForInfo))]
        public IHttpActionResult Postrequests(requestsForInfo requests)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.requestsForInfoes.Add(requests);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = requests.requestID}, requests);
        }

        // DELETE: api/RequestAPI/5
        [ResponseType(typeof(requestsForInfo))]
        public IHttpActionResult Deleterequests(int id)
        {
            requestsForInfo requests = db.requestsForInfoes.Find(id);
            if (requests == null)
            {
                return NotFound();
            }

            db.requestsForInfoes.Remove(requests);
            db.SaveChanges();

            return Ok(requests);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool requestsExists(int id)
        {
            return db.requestsForInfoes.Count(e => e.requestID == id) > 0;
        }
    }
}