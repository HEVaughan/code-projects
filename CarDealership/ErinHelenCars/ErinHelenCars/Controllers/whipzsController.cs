
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ErinHelenCars.Models;
using ErinHelenCars.CustomHtmlHelpers;

namespace ErinHelenCars.Controllers
{
    public class whipzsController : Controller
    {
        private HEEZcarsEntities db = new HEEZcarsEntities();       

        // GET: whipzs
        [Authorize(Roles ="Admin")]
        public ActionResult Index()
        {
            return View(db.whipzs.ToList());
        }

        public ActionResult List()
        {
            return View(db.whipzs.ToList());
        }


        // GET: whipzs/Details/5
        [Authorize(Roles ="Admin")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            whipz whipz = db.whipzs.Find(id);
            if (whipz == null)
            {
                return HttpNotFound();
            }
            return View(whipz);
        }

        // GET: whipzs/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult Create([Bind(Include = "vehicleID,make,model,year,mileage,adTitle,description,price,URLofPicture,isAvailable")] whipz whipz)
        {
            if (ModelState.IsValid)
            {
                db.whipzs.Add(whipz);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(whipz);
        }

        // GET: whipzs/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            whipz whipz = db.whipzs.Find(id);
            if (whipz == null)
            {
                return HttpNotFound();
            }
            return View(whipz);
        }

  
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult Edit([Bind(Include = "vehicleID,make,model,year,mileage,adTitle,description,price,URLofPicture,isAvailable")] whipz whipz)
        {
            if (ModelState.IsValid)
            {
                db.Entry(whipz).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(whipz);
        }

        // GET: whipzs/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            whipz whipz = db.whipzs.Find(id);
            if (whipz == null)
            {
                return HttpNotFound();
            }
            return View(whipz);
        }

        // POST: whipzs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult DeleteConfirmed(int id)
        {
            whipz whipz = db.whipzs.Find(id);
            db.whipzs.Remove(whipz);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

//﻿using System;
//using System.Collections.Generic;
//using System.Data;
//using System.Data.Entity;
//using System.Linq;
//using System.Net;
//using System.Web;
//using System.Web.Mvc;
//using ErinHelenCars.Models;

//namespace ErinHelenCars.Controllers
//{
//    public class whipzsController : Controller
//    {
//        private HEEZcarsEntities db = new HEEZcarsEntities();

//        // GET: whipzs
//        public ActionResult Index()
//        {
//            return View(db.whipzs.ToList());
//        }

//        // GET: whipzs/Details/5
//        public ActionResult Details(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            whipz whipz = db.whipzs.Find(id);
//            if (whipz == null)
//            {
//                return HttpNotFound();
//            }
//            return View(whipz);
//        }

//        // GET: whipzs/Create
//        public ActionResult Create()
//        {
//            return View();
//        }

//        // POST: whipzs/Create
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Create([Bind(Include = "vehicleID,make,model,year,mileage,adTitle,description,price,URLofPicture,isAvailable")] whipz whipz)
//        {
//            if (ModelState.IsValid)
//            {
//                db.whipzs.Add(whipz);
//                db.SaveChanges();
//                return RedirectToAction("Index");
//            }

//            return View(whipz);
//        }

//        // GET: whipzs/Edit/5
//        public ActionResult Edit(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            whipz whipz = db.whipzs.Find(id);
//            if (whipz == null)
//            {
//                return HttpNotFound();
//            }
//            return View(whipz);
//        }

//        // POST: whipzs/Edit/5
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Edit([Bind(Include = "vehicleID,make,model,year,mileage,adTitle,description,price,URLofPicture,isAvailable")] whipz whipz)
//        {
//            if (ModelState.IsValid)
//            {
//                db.Entry(whipz).State = EntityState.Modified;
//                db.SaveChanges();
//                return RedirectToAction("Index");
//            }
//            return View(whipz);
//        }

//        // GET: whipzs/Delete/5
//        public ActionResult Delete(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            whipz whipz = db.whipzs.Find(id);
//            if (whipz == null)
//            {
//                return HttpNotFound();
//            }
//            return View(whipz);
//        }

//        // POST: whipzs/Delete/5
//        [HttpPost, ActionName("Delete")]
//        [ValidateAntiForgeryToken]
//        public ActionResult DeleteConfirmed(int id)
//        {
//            whipz whipz = db.whipzs.Find(id);
//            db.whipzs.Remove(whipz);
//            db.SaveChanges();
//            return RedirectToAction("Index");
//        }

//        protected override void Dispose(bool disposing)
//        {
//            if (disposing)
//            {
//                db.Dispose();
//            }
//            base.Dispose(disposing);
//        }
//    }
//}

