﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ErinHelenCars.Models;

namespace ErinHelenCars.Controllers
{
    public class CarsAPIController : ApiController
    {
        private HEEZcarsEntities db = new HEEZcarsEntities();

        // GET: api/CarsAPI
        public IQueryable<whipz> Getwhipzs()
        {
            return db.whipzs;
        }

        // GET: api/CarsAPI/5
        [ResponseType(typeof(whipz))]
        public IHttpActionResult Getwhipz(int id)
        {
            whipz whipz = db.whipzs.Find(id);
            if (whipz == null)
            {
                return NotFound();
            }

            return Ok(whipz);
        }

        // PUT: api/CarsAPI/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putwhipz(int id, whipz whipz)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != whipz.vehicleID)
            {
                return BadRequest();
            }

            db.Entry(whipz).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!whipzExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/CarsAPI
        [ResponseType(typeof(whipz))]
        public IHttpActionResult Postwhipz(whipz whipz)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.whipzs.Add(whipz);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = whipz.vehicleID }, whipz);
        }

        // DELETE: api/CarsAPI/5
        [ResponseType(typeof(whipz))]
        public IHttpActionResult Deletewhipz(int id)
        {
            whipz whipz = db.whipzs.Find(id);
            if (whipz == null)
            {
                return NotFound();
            }

            db.whipzs.Remove(whipz);
            db.SaveChanges();

            return Ok(whipz);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool whipzExists(int id)
        {
            return db.whipzs.Count(e => e.vehicleID == id) > 0;
        }
    }
}