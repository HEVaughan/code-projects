﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ErinHelenCars.Models
{
    public interface ICarsRepository
    {
        IQueryable<whipz> GetAllCars();
        List<whipz> GetOrderedListofCars();
        whipz GetCar(int? id);
        void AddCar(whipz p);
    }
}
