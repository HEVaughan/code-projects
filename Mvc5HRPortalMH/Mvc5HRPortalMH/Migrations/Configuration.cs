namespace Mvc5HRPortalMH.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Mvc5HRPortalMH.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<Mvc5HRPortalMH.Models.PoliciesDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "Mvc5HRPortalMH.Models.PoliciesDBContext";
        }

        protected override void Seed(Mvc5HRPortalMH.Models.PoliciesDBContext context)
        {
            context.Policy.AddOrUpdate(i => i.PolicyName,
                new Policies
                {
                    Category = "Recruitment and appointments",
                    PolicyName = "Relocation",
                    PolicyDescription = "This policy aims to provide assistance to new appointees and existing employees to help with the expense of relocating to another locality."
                },

                 new Policies
                 {
                     Category = "Employment conditions",
                     PolicyName = "Code of Conduct",
                     PolicyDescription = "Minimum standards and obligations relating to the conduct and behavior expected of employees of SWG."
                 },

                  new Policies
                  {
                      Category = "Leave",
                      PolicyName = "Maternity Leave",
                      PolicyDescription = "Eligibilty and Conditions"
                  },

                   new Policies
                   {
                       Category = "Workplace health and safety",
                       PolicyName = "Emergency Procedures",
                       PolicyDescription = "Procedures to enable them to quickly and decisively respond to an actual or potential emergency"
                   }
        
  );
        }
    }
}
