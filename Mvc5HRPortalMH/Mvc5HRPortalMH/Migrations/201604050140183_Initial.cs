namespace Mvc5HRPortalMH.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Policies",
                c => new
                    {
                        PolicyNumber = c.Int(nullable: false, identity: true),
                        Category = c.String(),
                        PolicyName = c.String(),
                        PolicyDescription = c.String(),
                    })
                .PrimaryKey(t => t.PolicyNumber);

           

        }
        
        public override void Down()
        {
            DropTable("dbo.Policies");
            

        }
    }
}
