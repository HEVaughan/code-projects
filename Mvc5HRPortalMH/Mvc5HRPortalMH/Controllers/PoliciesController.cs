﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Mvc5HRPortalMH.Models;

namespace Mvc5HRPortalMH.Controllers
{
    public class PoliciesController : Controller
    {
        private PoliciesDBContext db = new PoliciesDBContext();

        // GET: Policies
        public ActionResult Index(string policyCategory, string searchString)
        {
            var CategoryList = new List<string>();

            var CategoryQry = from d in db.Policy
                           orderby d.Category
                           select d.Category;

            CategoryList.AddRange(CategoryQry.Distinct());
            ViewBag.policyCategory = new SelectList(CategoryList);

            var policies = from p in db.Policy
                         select p;

            if (!String.IsNullOrEmpty(searchString))
            {
                policies = policies.Where(s => s.PolicyName.Contains(searchString));
            }

            if (!string.IsNullOrEmpty(policyCategory))
            {
                policies = policies.Where(x => x.Category == policyCategory);
            }

            return View(policies.OrderBy(c => c.Category).ThenBy(x => x.PolicyName));


        }

        public ActionResult Display(string policyCategory, string searchString)
        {
            var CategoryList = new List<string>();

            var CategoryQry = from d in db.Policy
                              orderby d.Category
                              select d.Category;

            CategoryList.AddRange(CategoryQry.Distinct());
            ViewBag.policyCategory = new SelectList(CategoryList);

            var policies = from p in db.Policy
                           select p;

            if (!String.IsNullOrEmpty(searchString))
            {
                policies = policies.Where(s => s.PolicyName.Contains(searchString));
            }

            if (!string.IsNullOrEmpty(policyCategory))
            {
                policies = policies.Where(x => x.Category == policyCategory);
            }


            return View(policies.OrderBy(c => c.Category).ThenBy(x => x.PolicyName));
        }

        // GET: Policies/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Policies policies = db.Policy.Find(id);
            if (policies == null)
            {
                return HttpNotFound();
            }
            return View(policies);
        }

        // GET: Policies/Create
        public ActionResult Create(string policyCategory, string searchString)
        {
            var CategoryList = new List<SelectListItem>();

            var CategoryQry = from d in db.Policy
                              orderby d.Category
                              select d.Category;

            CategoryList.AddRange(CategoryQry.Distinct().Select(x => new SelectListItem { Selected = false, Text = x, Value = x }));
            ViewBag.policyCategory = new SelectList(CategoryList, "Value", "Text");

            var policies = from p in db.Policy
                           select p;

            if (!String.IsNullOrEmpty(searchString))
            {
                policies = policies.Where(s => s.PolicyName.Contains(searchString));
            }

            if (!string.IsNullOrEmpty(policyCategory))
            {
                policies = policies.Where(x => x.Category == policyCategory);
            }

            

                return View();
        }

        // POST: Policies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PolicyNumber,Category,PolicyName,PolicyDescription")] Policies policies)
        {
            if (ModelState.IsValid)
            {
                db.Policy.Add(policies);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(policies);
        }

        // GET: Policies/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Policies policies = db.Policy.Find(id);
            if (policies == null)
            {
                return HttpNotFound();
            }
            return View(policies);
        }

        // POST: Policies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PolicyNumber,Category,PolicyName,PolicyDescription")] Policies policies)
        {
            if (ModelState.IsValid)
            {
                db.Entry(policies).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(policies);
        }

        // GET: Policies/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Policies policies = db.Policy.Find(id);
            if (policies == null)
            {
                return HttpNotFound();
            }
            return View(policies);
        }

        // POST: Policies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Policies policies = db.Policy.Find(id);
            db.Policy.Remove(policies);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
