﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Mvc5HRPortalMH.Models
{
    public class JobApplications
    {
        [Key]
        public int JobApplicationNumber { get; set; }

        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Display(Name = "Resume")]
        public string UploadResume { get; set; }
    }

    public class JobApplicationsDBContext : DbContext
    {
        public DbSet<JobApplications> JobApplication { get; set; }
    }
}