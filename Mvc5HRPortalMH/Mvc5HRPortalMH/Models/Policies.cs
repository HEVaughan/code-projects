﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace Mvc5HRPortalMH.Models
{
    public class Policies
    {
        [Key]
        public int PolicyNumber { get; set; }

        [Display(Name = "Category")]
        public string Category { get; set; }
        [Display(Name = "Name")]
        public string PolicyName { get; set; }
        [Display(Name = "Description")]
        public string PolicyDescription { get; set; }
    }

        public class PoliciesDBContext : DbContext
        {
            public DbSet<Policies> Policy { get; set; }
        }
    }
