﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using DvdLibraryHJ.Repository;
using DvdLibraryHJ.Models;



namespace Dvd.Tests
{
    [TestFixture]
    public class MovieRepositoryTest
    {

        public MovieRepository _repo { get; set; }

        [SetUp]
        public void SetUp()
        {
            _repo = new MovieRepository();
        }

        [Test]
        public void GetAllMovieTest()
        {

            List<Movies> movies = _repo.GetAllMovies();

            Assert.AreEqual(1, movies.FirstOrDefault(m => m.ID == 1).ID);

        }

        [Test]
        public void SearchAllMovieTest()
        {

            var response = _repo.SearchMovies("Big Daddy");

            Assert.AreEqual(response, "Big Daddy");


        }

        //[Test]
        //public void DeleteMovieTest()
        //{

        //    Movies movie = _repo.DeleteMovie(18);

        //    Assert.IsNull(0);
        //}


    }
}
