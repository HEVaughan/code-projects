USE [Movies]
GO
/****** Object:  StoredProcedure [dbo].[AddNewMovieDetails]    Script Date: 4/8/2016 4:43:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER Procedure [dbo].[AddNewBorrowerDetails]  
(  
   
   @BorrowerName varchar (50),  
   @BorrowedDate date,
   @ReturnedDate date
   
)  
as  
begin  
   Insert into Movies values(@BorrowerName,@BorrowedDate,@ReturnedDate)  
End  
