﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DvdLibraryHJ.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BorrowerController.Tests
{
    [TestClass()]
    public class BorrowerControllerTests
    {
        [TestMethod()]
        public void TestDetailsView()
        {
                var controller = new DVDController();
                var result = controller.Details(2) as ViewResult;
                Assert.AreEqual("Details", result.ViewName);
        }
            
    }
}