﻿using Flooring.Data;
using Flooring.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace Flooring.BLL
{
    public class Manager
    {
        #region Interfaces
        public iRepository _repo;
        public List<Order> _orderList;

        public Manager()
        {
            if (ConfigurationManager.AppSettings["mode"] == "prodmode")
            {
                _repo = new ProdRepository();
            }
            else
            {
                _repo = new TestRepository();
            }
        }

        public Manager(string orderDate)
        {
            if (ConfigurationManager.AppSettings["mode"] == "prodmode")
            {
                _repo = new ProdRepository();
                _repo.MakeFilePath(orderDate);
            }
            else
            {
                _repo = new TestRepository();
            }
        }

        public Manager(iRepository repo)
        {
            _repo = repo;
        }
        #endregion

        public Response<Order> FinishOrder(Order currentOrder)
        {
            var response = new Response<Order>();
            var taxRepo = new TaxRepository();
            var stateTax = new TaxInfo();

            var productRepo = new ProductRepository();
            var currentProduct = new ProductInfo();

            try
            {

                var orderList = _repo.GetAllOrders();

                if (currentOrder.OrderNumber == 0)
                {
                    if (orderList.Count == 0)
                    {
                        currentOrder.OrderNumber = 1;
                    }
                    else
                        currentOrder.OrderNumber = orderList.Select(i => i.OrderNumber).Max() + 1;
                }

                stateTax = taxRepo.GetTaxRate(currentOrder.State);
                currentOrder.TaxRate = stateTax.TaxRate;

                currentOrder.StateFull = stateTax.StateName;

                currentProduct = productRepo.GetProduct(currentOrder.ProductType);
                currentOrder.LaborPerSquareFoot = currentProduct.LaborPerSquareFoot;
                currentOrder.CostPerSquareFoot = currentProduct.CostPerSquareFoot;

                currentOrder.LaborCost = currentOrder.Area * currentOrder.LaborPerSquareFoot;
                currentOrder.MaterialCost = currentOrder.Area * currentOrder.CostPerSquareFoot;

                currentOrder.Tax = (currentOrder.TaxRate / 100) * (currentOrder.LaborCost + currentOrder.MaterialCost);
                currentOrder.Total = currentOrder.LaborCost + currentOrder.MaterialCost + currentOrder.Tax;

                if (currentOrder.Total == 0)
                {
                    response.Message = "Order creation unsuccessful. Please try again.";
                    response.Success = false;
                }
                else
                {
                    response.Message = "Order successfully created.";
                    response.Success = true;
                    response.Data = currentOrder;
                }

            }

            catch (Exception ex)
            {
               
                response.Success = false;
                response.Message = "Exception.";
                ExceptionHandler.AddNewEx(ex);
                
            }

            return response;
        }

        public Response<Order> CreateOrder(Order currentOrder)
        {
            Response<Order> orderNumbers = new Response<Order>();

            try
            {
                _repo.MakeNewOrder(currentOrder);

                Order orderFound = _repo.LoadOrders(currentOrder.OrderNumber);

                if (orderFound.OrderNumber != currentOrder.OrderNumber)
                {
                    orderNumbers.Success = false;
                    orderNumbers.Message = "Order creation failed.";
                }

                else
                {
                    orderNumbers.Success = true;
                    orderNumbers.Message = "Order created successfully.";
                    orderNumbers.Data = orderFound;
                }
            }
            catch (Exception ex)
            {

                orderNumbers.Success = false;
                orderNumbers.Message = "Order creation failed. Please try again.";
                ExceptionHandler.AddNewEx(ex);
            }

            return orderNumbers;
        }

        public Response<List<Order>> GetOrderFile()
        {
            var response = new Response<List<Order>>();

            try
            {
                List<Order> order = _repo.LoadOrderFiles();


                if (order.Count == 0)
                {
                    response.Success = false;
                    response.Message = "No orders are available for that date.";
                }
                else
                {
                    response.Success = true;
                    response.Data = order;
                }
            }
            catch (Exception ex)
            {
               
                response.Success = false;
                response.Message = "There was an error.  Please try again later.";
                ExceptionHandler.AddNewEx(ex);

            }

            return response;
        }

        public Response<Order> GetOrder(int orderNumber)
        {
            var response = new Response<Order>();

            try
            {
                var order = _repo.LoadOrders(orderNumber);


                if (order == null)
                {
                    response.Success = false;
                    response.Message = "Order was not found!";
                }
                else
                {
                    response.Success = true;
                    response.Data = order;
                }
            }
            catch (Exception ex)
            {
               
                response.Success = false;
                response.Message = "There was an error.  Please try again later.";
                ExceptionHandler.AddNewEx(ex);
            }

            return response;
        }

        public Response<Order> RemoveOrder(Order ordRemoved)
        {
            var response = new Response<Order>();

            try
            {
                var order = _repo.GetAllOrders();

                if (order == null)
                {
                    response.Success = false;
                    response.Message = "Invalid Order!";
                }
                else
                {
                    response.Success = true;
                    response.Message = "Order has been deleted.";
                    _repo.RemoveOrd(ordRemoved);
                    return response;
                }
            }
            catch (Exception ex)
            {

                response.Success = false;
                response.Message = "There was an error.  Please try again.";
                ExceptionHandler.AddNewEx(ex);
            }
            return response;

        }

        public Response<Order> EditOrder(Order currentOrder, string currentDate)
        {
            var response = new Response<Order>();

            try
            {
                _repo.EditOrder(currentOrder);

                Order orderFound = _repo.LoadOrders(currentOrder.OrderNumber);

                if (orderFound.OrderNumber == currentOrder.OrderNumber)
                {
                    response.Message = "Order update successful.";
                    response.Success = true;
                    response.Data = currentOrder;
                }
                else
                {
                    response.Message = "Order update failed.";
                    response.Success = false;
                }
            }

            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.Success = false;
                ExceptionHandler.AddNewEx(ex);
            }


            return response;
        }

    }
}

