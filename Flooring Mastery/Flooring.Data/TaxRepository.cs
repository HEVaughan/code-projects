﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Flooring.Models;

namespace Flooring.Data
{
    public class TaxRepository
    {
        private const string FilePath = "../../DataFiles/Taxes.txt";
        string[] reader;

        public List<TaxInfo> GetTaxInfo()
        {
            List<TaxInfo> taxList = new List<TaxInfo>();
            reader = File.ReadAllLines(FilePath);

            for (int i = 1; i < reader.Length; i++)
            {
                var columns = reader[i].Split(',');

                var tax = new TaxInfo();

                tax.StateAbbreviation = columns[0];
                tax.StateName = columns[1];
                tax.TaxRate = decimal.Parse(columns[2]);


                taxList.Add(tax);

            }

            return taxList;

        }

        public TaxInfo GetTaxRate(string StateAbbreviation)
        {
            List<TaxInfo> taxes = GetTaxInfo();
            return taxes.FirstOrDefault(t => t.StateAbbreviation == StateAbbreviation);
        }


    }
}
