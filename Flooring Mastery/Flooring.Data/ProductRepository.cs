﻿using System.Collections.Generic;
using System.Linq;
using System.IO;
using Flooring.Models;

namespace Flooring.Data
{
    public class ProductRepository
    {
        private const string FilePath = "../../DataFiles/Products.txt";
        string[] reader;

        public List<ProductInfo> GetProductInfo()
        {
            List<ProductInfo> productList = new List<ProductInfo>();
            reader = File.ReadAllLines(FilePath);

            for (int i = 1; i < reader.Length; i++)
            {
                var columns = reader[i].Split(',');

                var product = new ProductInfo();

                product.ProductType = columns[0];
                product.CostPerSquareFoot = decimal.Parse(columns[1]);
                product.LaborPerSquareFoot = decimal.Parse(columns[2]);
                

                productList.Add(product);
            }

            return productList;
        }

        public ProductInfo GetProduct(string ProductType)
        {
            List<ProductInfo> products = GetProductInfo();
            return products.FirstOrDefault(t => t.ProductType == ProductType);
        }


    }
}
