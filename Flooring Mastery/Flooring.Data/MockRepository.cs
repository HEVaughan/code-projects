﻿using Flooring.Models;
using System.Collections.Generic;
using System.IO;

namespace Flooring.Data
{
    public class MockRepository
    {
        public static List<Order> orderList = new List<Order>();

        static MockRepository()
        {
            orderList = new List<Order>();

            string filePath = "../../DataFiles/Orders_01011901.txt";

            string[] reader;
            reader = File.ReadAllLines(filePath);

            for (int i = 1; i < reader.Length; i++)
            {
                var columns = reader[i].Split(',');

                var order = new Order();

                order.OrderNumber = int.Parse(columns[0]);
                order.CustomerName = columns[1];
                order.State = columns[2];
                order.TaxRate = decimal.Parse(columns[3]);
                order.ProductType = columns[4];
                order.Area = int.Parse(columns[5]);
                order.CostPerSquareFoot = decimal.Parse(columns[6]);
                order.LaborPerSquareFoot = decimal.Parse(columns[7]);
                order.MaterialCost = decimal.Parse(columns[8]);
                order.LaborCost = decimal.Parse(columns[9]);
                order.Tax = decimal.Parse(columns[10]);
                order.Total = decimal.Parse(columns[11]);
                order.StateFull = columns[12];

                orderList.Add(order);

            }

        }
    }
}
