﻿using System.Collections.Generic;
using System.Linq;
using Flooring.Models;
using System.IO;

namespace Flooring.Data
{
    public class ProdRepository : iRepository
    {
        private string filePath;
        public List<Order> orderList;
        public string currentDate;

        public void MakeFilePath(string currentDate)
        {
            filePath = string.Format("../../DataFiles/Orders_{0}.txt", currentDate);
        }

        public void MakeNewOrder(Order newOrder)
        {
            var orderList = GetAllOrders();

            orderList.Add(newOrder);

            OverwriteFile(orderList);
        }

        public void EditOrder(Order orderToEdit)
        {
            var orderList = new List<Order>();

            orderList = LoadOrderFiles();

            Order orderToRemove = orderList.Where(i => i.OrderNumber == orderToEdit.OrderNumber).First();

            orderList.Remove(orderToRemove);

            orderList.Add(orderToEdit);

            orderList = orderList.OrderBy(i => i.OrderNumber).ToList();

            OverwriteFile(orderList);
        }

        public void RemoveOrd(Order removeOrder)
        {
            var orders = this.GetAllOrders();
            List<Order> clone = new List<Order>();
            for (int i = 0; i < orders.Count; i++)
            {
                clone.Add(orders.ElementAt(i));
            }
            foreach (var order in clone)
            {
                if (removeOrder.OrderNumber == order.OrderNumber)
                {
                    orders.Remove(order);
                }
            }
            OverwriteFile(orders);
        }

        public List<Order> LoadOrderFiles()
        {
            var orderList = new List<Order>();

            if (File.Exists(filePath))
            {
                string[] reader;
                reader = File.ReadAllLines(filePath);

                for (int i = 1; i < reader.Length; i++)
                {
                    var columns = reader[i].Split(',');

                    var order = new Order();

                    order.OrderNumber = int.Parse(columns[0]);
                    order.CustomerName = columns[1];
                    order.State = columns[2];
                    order.TaxRate = decimal.Parse(columns[3]);
                    order.ProductType = columns[4];
                    order.Area = int.Parse(columns[5]);
                    order.CostPerSquareFoot = decimal.Parse(columns[6]);
                    order.LaborPerSquareFoot = decimal.Parse(columns[7]);
                    order.MaterialCost = decimal.Parse(columns[8]);
                    order.LaborCost = decimal.Parse(columns[9]);
                    order.Tax = decimal.Parse(columns[10]);
                    order.Total = decimal.Parse(columns[11]);
                    order.StateFull = columns[12];

                    orderList.Add(order);

                }
            }

            return orderList;
        }

        public List<Order> GetAllOrders()
        {
            var orderList = new List<Order>();

            if (!File.Exists(filePath))
            {

                using (var writer = File.CreateText(filePath))
                {
                    writer.WriteLine("OrderNumber,CustomerName,State,TaxRate,ProductType,Area,CostPerSquareFoot,LaborCostPerSquareFoot,MaterialCost,LaborCost,Tax,Total,FullState");
                }

            }
            else
            {
                orderList = LoadOrderFiles();
            }

            return orderList;

        }

        public Order LoadOrders(int orderNumber)
        {
            var orderList = new List<Order>();

            orderList = LoadOrderFiles();

            Order currentOrder = orderList.Where(i => i.OrderNumber == orderNumber).First();

            return currentOrder;
        }

        public void OverwriteFile(List<Order> orders)
        {
            File.Delete(filePath);

            using (var writer = File.CreateText(filePath))
            {
                writer.WriteLine("OrderNumber,CustomerName,State,TaxRate,ProductType,Area,CostPerSquareFoot,LaborCostPerSquareFoot,MaterialCost,LaborCost,Tax,Total,FullState");

                foreach (var order in orders)

                    writer.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12}", order.OrderNumber, order.CustomerName, order.State, order.TaxRate, order.ProductType, order.Area, order.CostPerSquareFoot, order.LaborPerSquareFoot, order.MaterialCost, order.LaborCost, order.Tax, order.Total, order.StateFull);
            }
        }
    }
}