﻿using Flooring.Models;
using System.Collections.Generic;

namespace Flooring.Data
{
    public interface iRepository
    {
        void MakeNewOrder(Order newOrder);
        void EditOrder(Order orderToEdit);
        void RemoveOrd(Order removeOrder);
        Order LoadOrders(int orderNumber);
        List<Order> LoadOrderFiles();
        List<Order> GetAllOrders();
        void MakeFilePath(string orderDate);
    }
}

