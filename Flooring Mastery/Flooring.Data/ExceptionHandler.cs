﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Flooring.Data
{
    public class ExceptionHandler
    {

        public static string FilePath = string.Format("../../DataFiles/ExceptionLog.txt");

        public static List<Exception> AddNewEx(Exception NewEx)
        {
            using (StreamWriter sw = File.AppendText(FilePath))
                {
                    sw.WriteLine("{0}, {1}, {2}, {3}",System.DateTime.Now, NewEx.TargetSite, NewEx.Message, NewEx.Source);
                    var ex = new List<Exception>();
                    return ex;
                }   

        }

    }
}
            