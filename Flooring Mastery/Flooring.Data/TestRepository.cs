﻿using System.Collections.Generic;
using System.Linq;
using Flooring.Models;

namespace Flooring.Data
{
    public class TestRepository : iRepository
    {
        public List<Order> orderList = MockRepository.orderList;

        public void MakeNewOrder(Order newOrder)
        {
            orderList.Add(newOrder);
        }

        public void EditOrder(Order orderToEdit)
        {
            Order orderToRemove = orderList.Where(i => i.OrderNumber == orderToEdit.OrderNumber).First();

            int indexPlace = orderList.IndexOf(orderToRemove);

            orderList.RemoveAt(indexPlace);

            orderList.Insert(indexPlace, orderToEdit);
        }

        public void RemoveOrd(Order removeOrder)
        {
            List<Order> clone = new List<Order>();

            for (int i = 0; i < orderList.Count; i++)
            {
                clone.Add(orderList.ElementAt(i));
            }

            foreach (var order in clone)
            {
                if (removeOrder.OrderNumber == order.OrderNumber)
                {
                    orderList.Remove(order);
                }
            }
        }

        public Order LoadOrders(int orderNumber)
        {
            Order currentOrder = orderList.Where(i => i.OrderNumber == orderNumber).First();

            return currentOrder;
        }

        public List<Order> LoadOrderFiles()
        {
            return orderList;
        }

        public List<Order> GetAllOrders()
        {
            return orderList;
        }

        public void MakeFilePath(string orderDate)
        {

        }

    }
}