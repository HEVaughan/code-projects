﻿namespace Flooring.Models
{
    public class ProductInfo
    {
        public string ProductType { get; set; }
        public decimal CostPerSquareFoot { get; set; }
        public decimal LaborPerSquareFoot { get; set; }
    }
}
