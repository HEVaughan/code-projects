﻿namespace Flooring.Models
{
    public class TaxInfo
    {
        public string StateName { get; set; }
        public string StateAbbreviation { get; set; }
        public decimal TaxRate { get; set; }
    }
}