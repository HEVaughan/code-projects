﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var db = new OrderDatabase();
            var list = db.GetAll();
            return View(list);

        }

        public ActionResult Add()
        {
            var model = new Order();
            return View(model);
        }

        public ActionResult Edit()
        {
            int orderId = int.Parse(RouteData.Values["id"].ToString());

            var db = new OrderDatabase();
            var order = db.GetById(orderId);

            return View(order);
        }

        [HttpPost]
        public ActionResult Edit(Order c)
        {
            var db = new OrderDatabase();
            db.Edit(c);

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult DeleteOrder()
        {
            int orderId = int.Parse(Request.Form["OrderId"]);

            var db = new OrderDatabase();
            db.Delete(orderId);

            var orders = db.GetAll();
            return View("Index", orders);
        }

        [HttpPost]
        public ActionResult AddOrder()
        {
            var c = new Order();

            c.CustomerName = Request.Form["Customer Name"];
            c.OrderDate = Request.Form["Order Date"];
            //c.State = Request.Form["Customer State of Residency"];
            //c.ProductType = Request.Form["Product Type"];
            

            var database = new OrderDatabase();
            database.Add(c);
            return RedirectToAction("Index");
        }

       

    }
}