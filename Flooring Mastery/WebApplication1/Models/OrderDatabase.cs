﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using Flooring.Models;

namespace WebApplication1.Models
{
    public class OrderDatabase
    {
        private static List<Order> _orders = new List<Order>();

        public List<Order> GetAll()
        {
            return _orders;
        }

        public void Add(Order order)
        {
            if (_orders.Any())
                order.OrderId = _orders.Max(c => c.OrderId) + 1;
            else
                order.OrderId = 1;
            _orders.Add(order);

        }

        public void Delete(int id)
        {
            _orders.RemoveAll(c => c.OrderId == id);
        }

        public void Edit(Order order)
        {
            Delete(order.OrderId);
            _orders.Add(order);
        }

        public Order GetById(int id)
        {
            return _orders.First(c => c.OrderId == id);
        }
    }
}