﻿using System.Linq;
using NUnit.Framework;
using Flooring.BLL;
using Flooring.Data;
using Flooring.Models;

namespace Flooring.Tests
{
    [TestFixture]
    public class OrderTests
    {
        [Test]
        public void RepoLoads()
        {
            Manager manager = new Manager(new TestRepository());
            TestData testRepo = new TestData();
            manager._orderList = testRepo.orderList;

            var response = manager.GetOrderFile();

            Assert.IsTrue(response.Success);
        }

        [TestCase(4, "Natasha Romanov")]
        [TestCase(3, "Tony Stark")]
        public void FoundOrder(int orderNumber, string customerName)
        {
            Manager manager = new Manager(new TestRepository());
            TestData testRepo = new TestData();
            manager._orderList = testRepo.orderList;

            var response = manager.GetOrder(orderNumber);

            Assert.IsTrue(response.Success);
            
        }

        [Test]
        public void FinishOrder()
        {
            Manager manager = new Manager(new TestRepository());
            TestData testRepo = new TestData();
            manager._orderList = testRepo.orderList;

            Order testOrder = new Order();

            testOrder.CustomerName = "Steve Rogers";
            testOrder.ProductType = "Tile";
            testOrder.Area = 100;
            testOrder.State = "OH";

            var response = manager.FinishOrder(testOrder);

            Assert.IsTrue(response.Success);
        }

        [TestCase(1, "Bruce Wayne", "Batman")]
        [TestCase(2, "Clark Kent", "Superman")]
        public void ChangeOrderInfo(int orderNumber, string originalName, string changedName)
        {
            Manager manager = new Manager(new TestRepository());
            TestData testRepo = new TestData();
            manager._orderList = testRepo.orderList;

            var orderList = manager._orderList;

            Order testOrder = orderList.Where(i => i.OrderNumber == orderNumber).First();
            originalName = testOrder.CustomerName;

            testOrder.CustomerName = changedName;

            var response = manager.EditOrder(testOrder, "01011901");

            Assert.AreNotEqual(changedName, originalName);

        }

        [Test]
        public void AddAnOrder()
        {
            Manager manager = new Manager(new TestRepository());
            TestData testRepo = new TestData();
            manager._orderList = testRepo.orderList;

            int preCount = manager.GetOrderFile().Data.Count;

            Order testOrder = new Order();
            testOrder.CustomerName = "Steve Rogers";

            var response = manager.CreateOrder(testOrder);

            int postCount = manager.GetOrderFile().Data.Count;

            Assert.AreEqual(postCount, preCount + 1);
        }

        [Test]
        public void DeleteAnOrder()
        {
            Manager manager = new Manager(new TestRepository());
            TestData testRepo = new TestData();
            manager._orderList = testRepo.orderList;

            int preCount = manager.GetOrderFile().Data.Count;

            var orderList = manager._orderList;
            Order testOrder = orderList.Where(i => i.OrderNumber == 1).First();

            var response = manager.RemoveOrder(testOrder);

            int postCount = manager.GetOrderFile().Data.Count;

            Assert.AreEqual(postCount, preCount - 1);
        }
    }
}
