﻿using NUnit.Framework;
using Flooring.BLL;
using Flooring.Models;
using Flooring.Data;

namespace Flooring.Tests
{
    [TestFixture]
    public class ProductTaxTests
    {
        [Test]
        public void TotalIsRight()
        {
            Manager manager = new Manager(new TestRepository());

            Order testOrder = new Order();

            testOrder.CustomerName = "Steve Rogers";
            testOrder.ProductType = "Tile";
            testOrder.Area = 100;
            testOrder.State = "OH";

            var response = manager.FinishOrder(testOrder);

            decimal expected = 1.0625M * (3.50M * 100 + 4.15M * 100);

            Assert.AreEqual(expected, response.Data.Total);
        }

        [TestCase("OH", 6.25)]
        [TestCase("PA", 6.75)]
        public void FoundTaxRate(string stateAbbr, decimal expected)
        {
            var taxRepo = new TaxRepository();
            var stateTax = new TaxInfo();

            stateTax = taxRepo.GetTaxRate(stateAbbr);
            decimal observed = stateTax.TaxRate;

            Assert.AreEqual(observed, expected);
        }

        [TestCase("Tile", 3.50)]
        [TestCase("Wood", 5.15)]
        public void FoundProductInfo(string product, decimal expected)
        {
            var productRepo = new ProductRepository();
            var productInfo = new ProductInfo();

            productInfo = productRepo.GetProduct(product);
            decimal observed = productInfo.CostPerSquareFoot;

            Assert.AreEqual(observed, expected);
        }

    }
}
