﻿using ConsoleApplication6.Utilities;
using Flooring.BLL;
using Flooring.Models;
using System;

namespace ConsoleApplication6.Workflows
{
    public class AddAnOrderWorkflow
    {
        private Order _newOrder = new Order();

        public void Execute()
        {
            Console.Write("\nEnter customer's full name: ");
            string name = Console.ReadLine();
            _newOrder.CustomerName = EntryValidation.CheckIsNotBlank(name, "Customer name");

            Console.WriteLine("\nEnter date of order: Press 'Enter' to use today's date or enter a date using");
            Console.Write("the format MM/DD/YYYY. ");
            string currentDate = Console.ReadLine().Trim();
            if (currentDate == "")
            {
                Console.SetCursorPosition(23, 16);
                _newOrder.OrderDate = DateTime.Now.ToString("MMddyyyy");
                Console.WriteLine(EntryValidation.FormatDate(_newOrder.OrderDate));
            }
                
            else
            {
                _newOrder.OrderDate = EntryValidation.ConvertDate(currentDate);
            }

            Console.WriteLine("\nState of Residence Choices");
            Console.WriteLine("1. Ohio");
            Console.WriteLine("2. Pennsylvania");
            Console.WriteLine("3. Michigan");
            Console.WriteLine("4. Indiana");
            Console.Write("Enter customer's state of residence by list number: ");
            string state = Console.ReadLine().Trim();
            _newOrder.State = EntryValidation.StateIsGood(state);

            Console.WriteLine("\nProduct Choices");
            Console.WriteLine("1. Carpet");
            Console.WriteLine("2. Laminate");
            Console.WriteLine("3. Tile");
            Console.WriteLine("4. Wood");
            Console.Write("Enter product type by list number: ");
            string product = Console.ReadLine().Trim();
            _newOrder.ProductType = EntryValidation.ProductFound(product);

            Console.Write("\nEnter flooring area: ");
            string floorArea = Console.ReadLine();
            _newOrder.Area = EntryValidation.AreaIsGood(floorArea);

            Manager _orderManager = new Manager(_newOrder.OrderDate);

            var orderResponse = _orderManager.FinishOrder(_newOrder);

            if (orderResponse.Success)
            {

                string keepOrder = OrderHandling.CompleteOrder(orderResponse);

                if (keepOrder == "Y")
                {
                    var creationResponse = _orderManager.CreateOrder(_newOrder);
                    Console.WriteLine("");
                    Console.WriteLine(creationResponse.Message);
                    Console.WriteLine("Press any key to continue.");
                    Console.ReadKey();
                }
            }

            else
            {
                Console.Clear();
                Console.WriteLine(orderResponse.Message);
                Console.WriteLine("Press any key to continue.");
                Console.ReadKey();
            }

        }


    }
}
