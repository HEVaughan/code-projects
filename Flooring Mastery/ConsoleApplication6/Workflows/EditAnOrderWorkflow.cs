﻿using ConsoleApplication6.Utilities;
using Flooring.BLL;
using Flooring.Models;
using System;
using System.Configuration;

namespace ConsoleApplication6.Workflows
{
    public class EditAnOrderWorkflow
    {
        Manager _orderManager;

        public void Execute()
        {
            string dateToEdit;

            if (ConfigurationManager.AppSettings["mode"] == "prodmode")
            {
                dateToEdit = OrderHandling.GetInfoFromUser();
                _orderManager = new Manager(dateToEdit);
            }
            else
            {
                dateToEdit = "01011901";
                _orderManager = new Manager();
            }

            OrderHandling.DisplayOrder(_orderManager, dateToEdit);

            Console.WriteLine("\nWhich order would you like to edit?");
            Console.Write("Enter an order number from the list above: ");
            string orderToEdit = Console.ReadLine().Trim();
            bool editMode = true;

            do
            {
                int orderNumber = EntryValidation.OrderNumberToInt(orderToEdit);

                var foundOrderResponse = _orderManager.GetOrder(orderNumber);

                if (foundOrderResponse.Success)
                {
                    Order currentOrder = foundOrderResponse.Data;

                    string formattedDate = EntryValidation.FormatDate(dateToEdit);
                    Console.Clear();
                    Console.WriteLine("Editing Order #{0} from {1}", orderNumber, formattedDate);
                    Console.WriteLine("\nINSTRUCTIONS:");
                    Console.WriteLine("Each user-editable field will be displayed with its current value.");
                    Console.WriteLine("Make your change to that field then press enter.");
                    Console.WriteLine("To keep the current value, press enter to move to the next field.\n");

                    currentOrder = EditOrder(currentOrder);

                    var orderResponse = _orderManager.FinishOrder(currentOrder);

                    string keepOrder = OrderHandling.CompleteOrder(orderResponse);

                    if (keepOrder == "Y")
                    {
                        var editResponse = _orderManager.EditOrder(orderResponse.Data, dateToEdit);
                        Console.WriteLine("");
                        Console.WriteLine(editResponse.Message);
                        Console.WriteLine("Press any key to continue.");
                        Console.ReadKey();
                    }

                    editMode = false;

                }
                else
                {
                    Console.Write("Invalid order number. Please enter an order number from the list: ");
                    orderToEdit = Console.ReadLine().Trim();
                    editMode = true;
                }
            } while (editMode == true);

        }

        public Order EditOrder(Order currentOrder)
        {
            Order editedOrder = MakeCopy(currentOrder);

            Console.Write("Customer's name ({0}): ", currentOrder.CustomerName);
            string newName = Console.ReadLine().Trim();
            if (newName != "")
                editedOrder.CustomerName = EntryValidation.CheckIsNotBlank(newName, "Customer name");

            Console.WriteLine("\nState of Residence Choices\n(Enter your choice by list number)");
            Console.WriteLine("1. Ohio");
            Console.WriteLine("2. Pennsylvania");
            Console.WriteLine("3. Michigan");
            Console.WriteLine("4. Indiana");
            Console.Write("Customer's state of residence ({0}): ", currentOrder.StateFull);
            string newState = Console.ReadLine().Trim();
            if (newState != "")
                editedOrder.State = EntryValidation.StateIsGood(newState);

            Console.WriteLine("\nProduct Choices\n(Enter your choice by list number)");
            Console.WriteLine("1. Carpet");
            Console.WriteLine("2. Laminate");
            Console.WriteLine("3. Tile");
            Console.WriteLine("4. Wood");
            Console.Write("Product choice ({0}): ", currentOrder.ProductType);
            string newProduct = Console.ReadLine().Trim();
            if (newProduct != "")
                editedOrder.ProductType = EntryValidation.ProductFound(newProduct);

            Console.Write("\nFlooring area ({0}): ", currentOrder.Area);
            string newArea = Console.ReadLine().Trim();
            if (newArea != "")
                editedOrder.Area = EntryValidation.AreaIsGood(newArea);

            return editedOrder;
        }

        public Order MakeCopy(Order currentOrder)
        {
            Order orderCopy = new Order();
            orderCopy.Area = currentOrder.Area;
            orderCopy.CustomerName = currentOrder.CustomerName;
            orderCopy.OrderDate = currentOrder.OrderDate;
            orderCopy.OrderNumber = currentOrder.OrderNumber;
            orderCopy.ProductType = currentOrder.ProductType;
            orderCopy.State = currentOrder.State;

            return orderCopy;
        }
    }
}
