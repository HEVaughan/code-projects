﻿using System;

namespace ConsoleApplication6.Workflows
{
    public class MainMenu
    {
        public void Execute()
        {
            do
            {
                Console.Clear();
                Console.WriteLine("   SWC FLOORING   ");
                Console.WriteLine("====================");
                Console.WriteLine("\n1. Display Orders");
                Console.WriteLine("2. Add an Order");
                Console.WriteLine("3. Edit an Order");
                Console.WriteLine("4. Remove an Order");
                Console.WriteLine("\n5. Quit");

                Console.Write("\n\nEnter Choice: ");
                string input = Console.ReadLine();

                if (input.Substring(0, 1) == "5")
                    break;

                ProcessChoice(input);

            } while (true);

        }


        public void ProcessChoice(string input)
        {
            switch (input)
            {
                case "1":
                    var display = new DisplayOrdersWorkflow();
                    display.Execute();
                    break;
                case "2":
                    var addOrder = new AddAnOrderWorkflow();
                    addOrder.Execute();
                    break;
                case "3":
                    var editOrder = new EditAnOrderWorkflow();
                    editOrder.Execute();
                    break;
                case "4":
                    var removeOrder = new RemoveAnOrderWorkflow();
                    removeOrder.Execute();
                    break;

            }
        }




    }
}
