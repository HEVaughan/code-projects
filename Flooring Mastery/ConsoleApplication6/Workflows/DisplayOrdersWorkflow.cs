﻿using ConsoleApplication6.Utilities;
using Flooring.BLL;
using System;
using System.Configuration;

namespace ConsoleApplication6.Workflows
{
    public class DisplayOrdersWorkflow
    {
        Manager _orderManager = new Manager();

        public void Execute()
        {
            string order;
            if (ConfigurationManager.AppSettings["mode"] == "prodmode")
            {
                order = OrderHandling.GetInfoFromUser();
                _orderManager = new Manager(order);
            }

            else
            {
                order = "01011901";
                _orderManager = new Manager();
            }

            OrderHandling.DisplayOrder(_orderManager, order);

            Console.WriteLine("\nPress any key to continue.");
            Console.ReadKey();
        }
    }

}

