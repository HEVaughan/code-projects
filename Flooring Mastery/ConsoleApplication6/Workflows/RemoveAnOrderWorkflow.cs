﻿using ConsoleApplication6.Utilities;
using Flooring.BLL;
using System;
using System.Configuration;

namespace ConsoleApplication6.Workflows
{
    public class RemoveAnOrderWorkflow
    {
        Manager _orderManager;

        public void Execute()
        {
            string ordNum;
            if (ConfigurationManager.AppSettings["mode"] == "prodmode")
            {
                ordNum = OrderHandling.GetInfoFromUser();
                _orderManager = new Manager(ordNum);
            }
            else
            {
                ordNum = "01011901";
                _orderManager = new Manager();
            }

            OrderHandling.DisplayOrder(_orderManager, ordNum);


            Console.WriteLine("Which order would you like to remove?");
            Console.Write("Enter an order number from the list above: ");
            string removeRequest = Console.ReadLine().Trim();
            string input;

            do
            {
                Console.Clear();
                int orderNumber = EntryValidation.OrderNumberToInt(removeRequest);
                var removeResponse = _orderManager.GetOrder(orderNumber);
                OrderHandling.PrintOrderDetails(removeResponse.Data);
                Console.Write("\n\nAre you sure you want to delete this order? Y/N: ");
                input = Console.ReadLine().ToUpper();


                if (input == "Y")
                {
                    var orderToRemove = _orderManager.RemoveOrder(removeResponse.Data);
                    Console.WriteLine("");
                    Console.WriteLine(orderToRemove.Message);
                    Console.WriteLine("Press any key to continue");
                    Console.ReadLine();
                }


            } while (input != "Y" && input != "N");

        }

    }
}
