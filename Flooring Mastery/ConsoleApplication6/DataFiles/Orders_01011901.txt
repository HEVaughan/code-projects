OrderNumber,CustomerName,State,TaxRate,ProductType,Area,CostPerSquareFoot,LaborCostPerSquareFoot,MaterialCost,LaborCost,Tax,Total,FullState
1,Bruce Wayne,OH,6.25,Tile,10000,3.50,4.15,35000.00,41500.00,4781.250000,81281.250000,Ohio
2,Clark Kent,PA,6.75,Laminate,100,1.75,2.10,175.00,210.00,25.987500,410.987500,Pennsylvania
3,Tony Stark,MI,5.75,Wood,2500,5.15,4.75,12875.00,11875.00,1423.125000,26173.125000,Michigan
4,Natasha Romanov,IN,6.00,Carpet,500,2.25,2.10,1125.00,1050.00,130.5000,2305.5000,Indiana
