﻿using Flooring.BLL;
using Flooring.Models;
using System;
using System.Configuration;

namespace ConsoleApplication6.Utilities
{
    public class OrderHandling
    {
        public static string CompleteOrder(Response<Order> orderResponse)
        {
            Console.Clear();
            PrintOrderDetails(orderResponse.Data);

            Console.WriteLine("\nWould you like to complete this order?");
            Console.WriteLine("WARNING: Responding 'no' will discard all changes.");
            Console.Write("Y/N: ");

            string keepOrder;

            do
            {
                keepOrder = Console.ReadLine().Trim();
                while (keepOrder == "")
                {
                    Console.Write("Entry cannot be blank. Do you want to complete this order? ");
                    keepOrder = Console.ReadLine().Trim();
                }

                keepOrder = keepOrder.Substring(0, 1).ToUpper();

                if (keepOrder != "Y" && keepOrder != "N")
                    Console.Write("Invalid choice. Do you want to complete this order? ");
            } while (keepOrder != "Y" && keepOrder != "N");

            return keepOrder;
        }

        public static void DisplayOrder(Manager _orderManager, string order)
        {
            var response = _orderManager.GetOrderFile();

            string formattedDate = EntryValidation.FormatDate(order);

            Console.Clear();

            if (response.Success)
            {
                if (ConfigurationManager.AppSettings["mode"] == "prodmode")
                    Console.WriteLine("   Orders for {0}", formattedDate);
                else
                    Console.WriteLine("   Current Orders");

                Console.WriteLine("=============================\n\n");
                foreach (var r in response.Data)
                {
                    PrintOrderDetails(r);
                    Console.WriteLine("");
                }

            }
            else
            {
                Console.WriteLine(response.Message);
            }

        }

        public static void PrintOrderDetails(Order response)
        {
            Console.WriteLine("      Order Number: {0}   ", response.OrderNumber);
            Console.WriteLine("-----------------------------");
            Console.WriteLine("Customer Name: {0}", response.CustomerName);
            Console.WriteLine("State: {0}", response.State);
            Console.WriteLine("Tax Rate: {0}%", response.TaxRate);
            Console.WriteLine("Product: {0}", response.ProductType);
            Console.WriteLine("Area: {0}", response.Area);
            Console.WriteLine("Cost per square foot: {0:c}", response.CostPerSquareFoot);
            Console.WriteLine("Labor per square foot: {0:c}", response.LaborPerSquareFoot);
            Console.WriteLine("Material cost: {0:c}", response.MaterialCost);
            Console.WriteLine("Labor Cost: {0:c}", response.LaborCost);
            Console.WriteLine("Tax: {0:c}", response.Tax);
            Console.WriteLine("Total: {0:c}", response.Total);
        }

        public static string GetInfoFromUser()
        {
            Console.WriteLine("\nEnter date of order: Press 'Enter' to use today's date or enter a date using");
            Console.Write("the format MM/DD/YYYY. ");
            string orderDate = Console.ReadLine().Trim();

            if (orderDate == "")
                orderDate = DateTime.Now.ToString("MMddyyyy");
            else
                orderDate = EntryValidation.ConvertDate(orderDate);

            return orderDate;

        }
    }
}
