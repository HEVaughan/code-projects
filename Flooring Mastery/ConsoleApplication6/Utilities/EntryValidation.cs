﻿using System;

namespace ConsoleApplication6.Utilities
{
    public class EntryValidation
    {
        public static string CheckIsNotBlank(string input, string infoType)
        {
            while (input.Trim() == "")
            {
                Console.WriteLine("{0} cannot be blank. Please re-enter this information.", infoType);
                Console.Write("{0}: ", infoType);
                input = Console.ReadLine();
            }

            return input;
        }

        public static string ConvertDate(string input)
        {
            bool dateIsGood = false;
            do
            {
                input = input.Replace("/", "");

                while (input.Length != 8)
                {
                    Console.Write("Invalid date format. Please re-enter the date: ");
                    input = Console.ReadLine().Trim();
                    if (input == "")
                        input = DateTime.Now.ToString("MMddyyyy");
                    else
                        input = input.Replace("/", "");               
                }

                int month;
                int day;
                bool monthIsGood = int.TryParse(input.Substring(0, 2), out month);
                bool dayIsGood = int.TryParse(input.Substring(2, 2), out day);

                if (monthIsGood && dayIsGood)
                {
                    if (month >= 1 && month <= 12 && day >= 1 && day <= 31)
                        dateIsGood = true;
                }

                else
                {
                    Console.Write("Unrecognized date. Please re-enter the date: ");
                    input = Console.ReadLine();
                }

            } while (dateIsGood == false);

            return input;
        }

        public static int AreaIsGood(string floorArea)
        {
            int area;
            bool areaCheck = true;
            bool isPositive = true;
            do
            {
                do
                {
                    areaCheck = int.TryParse(floorArea, out area);
                    if (!areaCheck)
                    {
                        Console.Write("Area is not a valid number. Please re-enter the area: ");
                        floorArea = Console.ReadLine().Trim();
                    }

                } while (!areaCheck);

                if (area < 0)
                    isPositive = false;

            } while (isPositive == false);

            return area;
        }

        public static string StateIsGood(string state)
        {
            StateList customerState = new StateList();
            string stateName;
            do
            {
                while (state != "1" && state != "2" && state != "3" && state != "4")
                {
                    Console.Write("Invalid state entry. Please re-enter the state's list number: ");
                    state = Console.ReadLine().Trim();
                }

                bool goodState = Enum.IsDefined(typeof(StateList), int.Parse(state));
                if (goodState)
                {
                    customerState = (StateList)Enum.Parse(typeof(StateList), state);
                    stateName = customerState.ToString();
                    break;
                }
                else
                {
                    Console.Write("State not recognized. Please re-enter the customer's state: ");
                    state = Console.ReadLine().ToUpper();
                }
            } while (true);

            return stateName;
        }

        public static string ProductFound (string product)
        {
            ProductList customerProduct = new ProductList();
            string productName;
            do
            {
                while (product != "1" && product != "2" && product != "3" & product != "4")
                {
                    Console.Write("Invalid product number. Please re-enter the product number: ");
                    product = Console.ReadLine().Trim();
                }

                bool goodProduct = Enum.IsDefined(typeof(ProductList), int.Parse(product));
                if (goodProduct)
                {
                    customerProduct = (ProductList)Enum.Parse(typeof(ProductList), product);
                    productName = customerProduct.ToString();
                    break;
                }
                else
                {
                    Console.Write("Product not recognized. Please re-enter the product to order: ");
                    product = Console.ReadLine().ToLower();
                }
            } while (true);

            return productName;
        }

        public static string FormatDate(string date)
        {
            string formattedDate = string.Format("{0}/{1}/{2}",date.Substring(0,2), date.Substring(2,2), date.Substring(4));

            return formattedDate;
        }

        public static int OrderNumberToInt(string orderToEdit)
        {
            int orderNumber;
            bool canConvert;

            do
            {
                canConvert = int.TryParse(orderToEdit, out orderNumber);
                if (canConvert == true)
                    break;
                else
                {
                    Console.Write("Invalid entry. Please enter an available order number: ");
                    orderToEdit = Console.ReadLine().Trim();
                }

            } while (true);

            return orderNumber;

        }
    }
}
