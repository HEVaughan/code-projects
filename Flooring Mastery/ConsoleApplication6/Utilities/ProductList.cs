﻿namespace ConsoleApplication6.Utilities
{
    public enum ProductList
    {
        Carpet = 1,
        Laminate = 2,
        Tile = 3,
        Wood = 4
    }
}
