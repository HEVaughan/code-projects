﻿using ConsoleApplication6.Workflows;

namespace ConsoleApplication6
{
    class Program
    {
        static void Main(string[] args)
        {
            var menu = new MainMenu();
            menu.Execute();
        }
    }
}
