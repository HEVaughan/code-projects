﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseballModel
{
    public interface IBaseballRepository
    {
        IQueryable<Player> GetAllPlayers();
        List<Player> GetOrderedListofPlayers();
        Player GetPlayer(int? id);
        void AddPlayer(Player p);

        IQueryable<Team> GetAllTeams();
        List<Team> GetOrderedListofTeams();
        Team GetTeam(int? id);
        void AddTeam(Team p);
    }
}
