﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace BaseballModel
{
    public class BaseballRepository : IBaseballRepository
    {
        BaseballEntities ctx;

        public BaseballRepository(BaseballEntities entities)
        {
            ctx = entities;
        }

        public void AddPlayer(Player p)
        {
            ctx.Players.Add(p);
            ctx.SaveChanges();
        }

        public IQueryable<Player> GetAllPlayers()
        {
            return ctx.Players;
        }

        public List<Player> GetOrderedListofPlayers()
        {
            return ctx.Players.Include(p => p.Team).OrderBy(p => p.Team.TeamName).ThenBy(p => p.PlayerName).ToList();
        }

        //public Player GetPlayer(int? id)
        //{
        //    throw new NotImplementedException();
        //}

        public Player GetPlayer(int? id)
        {
            return ctx.Players.First(x => x.PlayerID == id);
        }

        public void AddTeam(Team p)
        {
            ctx.Teams.Add(p);
            ctx.SaveChanges();
        }

        public IQueryable<Team> GetAllTeams()
        {
            return ctx.Teams;
        }

        public List<Team> GetOrderedListofTeams()
        {
            return ctx.Teams.OrderBy(p => p.TeamName).ToList();
        }

        //public Player GetPlayer(int? id)
        //{
        //    throw new NotImplementedException();
        //}

        public Team GetTeam(int? id)
        {
            return ctx.Teams.First(x => x.TeamID == id);
        }
    }
}
