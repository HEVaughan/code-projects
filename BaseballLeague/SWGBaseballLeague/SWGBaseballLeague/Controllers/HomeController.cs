﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BaseballModel;
using SWGBaseballLeague.Models;

namespace SWGBaseballLeague.Controllers
{
    public class HomeController : Controller
    {
        private BaseballEntities db = new BaseballEntities();
     
        public ActionResult Index()
        {
            return View(db.Teams.ToList());
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}