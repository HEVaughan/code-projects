﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BaseballModel;
using SWGBaseballLeague.Models;

namespace SWGBaseballLeague.Controllers
{
    public class TeamInfoController : Controller
    {
        private BaseballEntities db = new BaseballEntities();
        private IBaseballRepository _repo;

        public TeamInfoController(IBaseballRepository repo)
        {
            _repo = repo;
        }

        // GET: TeamInfo
        public ActionResult Index()
        {
            var teams = _repo.GetOrderedListofTeams();
            return View(teams.ToList());
        }

        //public ActionResult Roster()
        //{
        //    var Roster = from p in db.Players
        //                 where p.TeamID == p.TeamID
        //                 select p;
        //}



        //return View(Roster);

        // GET: TeamInfo/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TeamInfo teamInfo = Mapping.ConvertEntityToModel(db.Teams.Find(id));
            if (teamInfo == null)
            {
                return HttpNotFound();
            }
            return View(teamInfo);
        }

        // GET: TeamInfo/Create
        public ActionResult Create()
        {
            return View();
        }

        //// POST: TeamInfo/Create
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Details()
        //{

        //    return RedirectToAction("PlayerInfo.Index");
        //}

        // POST: TeamInfo/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TeamID,Manager,TeamName")] TeamInfo teamInfo)
        {
            if (ModelState.IsValid)
            {
                db.Teams.Add(Mapping.ConvertEntityToModel(teamInfo));
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(teamInfo);
        }

        // GET: TeamInfo/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TeamInfo teamInfo = Mapping.ConvertEntityToModel(db.Teams.Find(id));
            if (teamInfo == null)
            {
                return HttpNotFound();
            }
            return View(teamInfo);
        }

        // POST: TeamInfo/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TeamID,Manager,TeamName")] Team teamInfo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(teamInfo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(teamInfo);
        }

        // GET: TeamInfo/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Team teamInfo = db.Teams.Find(id);
            if (teamInfo == null)
            {
                return HttpNotFound();
            }
            return View(teamInfo);
        }

        // POST: TeamInfo/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Team teamInfo = db.Teams.Find(id);
            db.Teams.Remove(teamInfo);
            db.SaveChanges();
            return RedirectToAction("Index");

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
