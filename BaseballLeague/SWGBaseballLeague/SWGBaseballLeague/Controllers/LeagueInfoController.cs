﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BaseballModel;
using SWGBaseballLeague.Models;

namespace SWGBaseballLeague.Controllers
{
    public class LeagueInfoController : Controller
    {
        private BaseballEntities db = new BaseballEntities();

        // GET: LeagueInfo
        public ActionResult Index()
        {
            return View(db.Leagues.ToList());
        }

        // GET: LeagueInfo/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
             
            League leagueInfo = db.Leagues.Find(id);
            if (leagueInfo == null)
            {
                return HttpNotFound();
            }
            return View(leagueInfo);
        }

        // GET: LeagueInfo/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: LeagueInfo/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "LeagueID,LeagueName")] League leagueInfo)
        {
            if (ModelState.IsValid)
            {
                db.Leagues.Add(leagueInfo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(leagueInfo);
        }

        // GET: LeagueInfo/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            League leagueInfo = db.Leagues.Find(id);
            if (leagueInfo == null)
            {
                return HttpNotFound();
            }
            return View(leagueInfo);
        }

        // POST: LeagueInfo/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "LeagueID,LeagueName")] League leagueInfo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(leagueInfo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(leagueInfo);
        }

        // GET: LeagueInfo/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            League leagueInfo = db.Leagues.Find(id);
            if (leagueInfo == null)
            {
                return HttpNotFound();
            }
            return View(leagueInfo);
        }

        // POST: LeagueInfo/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            League leagueInfo = db.Leagues.Find(id);
            db.Leagues.Remove(leagueInfo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
