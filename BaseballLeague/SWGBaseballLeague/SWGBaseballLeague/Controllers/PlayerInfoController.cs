﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BaseballModel;
using SWGBaseballLeague.Models;


namespace SWGBaseballLeague.Controllers
{
    public class PlayerInfoController : Controller
    {
        private BaseballEntities db = new BaseballEntities();
        private IBaseballRepository _repo;

        public PlayerInfoController(IBaseballRepository repo)
        {
            _repo = repo;
        }

        // GET: PlayerInfo
        public ActionResult Index(int? id)
        {
            var players = _repo.GetOrderedListofPlayers().Where(x => x.TeamID == id);
            return View(players.ToList());
                 
        }
        //var Roster = from p in db.Players
        //             where p.TeamID == p.TeamID
        //             select p;


        //return View(Roster);
        //return View();


        // GET: PlayerInfo/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Player player = _repo.GetPlayer(id);
            if (player == null)
            {
                return HttpNotFound();
            }
            return View(player);
        }
        //            
        // GET: PlayerInfo/Create
        public ActionResult Create()
        {

            var teams = from t in db.Teams
                        select t;

            var items = new List<SelectListItem>();

            foreach (var team in teams)
            {
                items.Add(new SelectListItem() { Selected = false, Text = team.TeamName, Value = team.TeamID.ToString() });
            }

            ViewBag.List = new SelectList(items, "Value", "Text", 1);

            return View(new Player());
            //return View();

        }


        // POST: PlayerInfo/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Player playerInfo)
        {
            if (ModelState.IsValid)
            {
                _repo.AddPlayer(playerInfo);
                return RedirectToAction("Index");
            }

            ViewBag.TeamID = new SelectList(db.Teams, "TeamID", "TeamName", playerInfo.TeamID);
            return View(playerInfo);
        }

        // GET: PlayerInfo/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Player playerInfo = db.Players.Find(id);
            if (playerInfo == null)
            {
                return HttpNotFound();
            }
            return View(playerInfo);
        }

        // POST: PlayerInfo/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PlayerID,Position,PlayerNumber,PlayerName,BattingAvg,YearsPlayed")] Player playerInfo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(playerInfo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(playerInfo);
        }

        // GET: PlayerInfo/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Player playerInfo = db.Players.Find(id);
            if (playerInfo == null)
            {
                return HttpNotFound();
            }
            return View(playerInfo);
        }

        // POST: PlayerInfo/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Player playerInfo = db.Players.Find(id);
            db.Players.Remove(playerInfo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
