﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using BaseballModel;

namespace SWGBaseballLeague.Models
{
    public class LeagueInfo
    {
        [Key]
        public int LeagueID { get; set; }
        [Display(Name = "League Name")]
        public string LeagueName { get; set; }
        public List<Team> Teams { get; set; }

        public LeagueInfo()
        {
            Teams = new List<Team>();
        }
    }
}