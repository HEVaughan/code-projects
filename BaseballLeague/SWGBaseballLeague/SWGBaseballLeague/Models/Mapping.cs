﻿using BaseballModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SWGBaseballLeague.Models
{
    public static class Mapping
    {
        public static LeagueInfo ConvertEntityToModel(League from)
        {
            LeagueInfo to = new LeagueInfo();
            to.LeagueID = from.LeagueID;
            to.LeagueName = from.LeagueName;
            //foreach (var t in from.Teams)
            //to.Teams.Add(ConvertEntityToModel(t));
            return to;
        }

        public static PlayerInfo ConvertEntityToModel(Player from)
        {
            PlayerInfo to = new PlayerInfo();
            to.PlayerID = from.PlayerID;
            to.PlayerName = from.PlayerName;
            to.PlayerNumber = from.PlayerNumber;
            to.Position = from.Position;
            to.BattingAvg = from.BattingAvg;
            to.YearsPlayed = from.YearsPlayed;
            return to;

        }

        public static TeamInfo ConvertEntityToModel(Team from)
        {
            TeamInfo to = new TeamInfo();
            to.TeamID = from.TeamID;
            to.TeamName = from.TeamName;
            to.Manager = from.Manager;
            return to;
        }

        public static League ConvertEntityToModel(LeagueInfo from)
        {
            League to = new League();
            to.LeagueID = from.LeagueID;
            to.LeagueName = from.LeagueName;
            //foreach (var t in from.Teams)
            //    to.Teams.Add(ConvertEntityToModel(t));
            return to;
        }

        public static Player ConvertEntityToModel(PlayerInfo from)
        {
            Player to = new Player();
            to.PlayerID = from.PlayerID;
            to.PlayerName = from.PlayerName;
            to.PlayerNumber = from.PlayerNumber;
            to.Position = from.Position;
            to.BattingAvg = from.BattingAvg;
            to.YearsPlayed = from.YearsPlayed;
            return to;

        }

        public static Team ConvertEntityToModel(TeamInfo from)
        {
            Team to = new Team();
            to.TeamID = from.TeamID;
            to.TeamName = from.TeamName;
            to.Manager = from.Manager;
            return to;
        }

    }
}