﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BaseballModel;
using System.Web.Mvc;

namespace SWGBaseballLeague.Models
{
    public class ViewModel
    {
        
            public Player Player { get; set; }
            public Team Team { get; set; }
        List<Player> _playerList = new List<Player>();
        List<Team> _teamList = new List<Team>();
        public List<Team> TeamList { get; set; }
        public IEnumerable<SelectListItem> DropDownItems { get; set; }
        
    }
}