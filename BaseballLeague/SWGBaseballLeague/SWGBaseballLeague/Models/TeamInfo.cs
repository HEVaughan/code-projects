﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using BaseballModel;

namespace SWGBaseballLeague.Models
{
    public class TeamInfo
    {
        [Key]
        public int TeamID { get; set; }
        public League League { get; set; }
        public string Manager { get; set; }
        [Display(Name = "Team")]
        public string TeamName { get; set; }
        public List<Player> Players { get; set; }

        public TeamInfo()
        {
            League = new League();
            Players = new List<Player>();
        }

    }
}