﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using BaseballModel;

namespace SWGBaseballLeague.Models
{
    public class PlayerInfo
    {
        [Key]
        public int PlayerID { get; set; }
        public Team Team { get; set; }
        public string Position { get; set; }
        [Display(Name = "#")]
        public int? PlayerNumber { get; set; }
        [Display(Name = "Player")]
        public string PlayerName { get; set; }
        [Display(Name = "Batting Average")]
        public decimal? BattingAvg { get; set; }
        [Display(Name = "Years Played")]
        public int? YearsPlayed { get; set; }

        public PlayerInfo()
        {
            Team = new Team();
        }
    }
}