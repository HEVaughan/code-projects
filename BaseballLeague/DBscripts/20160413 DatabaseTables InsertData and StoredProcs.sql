USE [master]
GO

/****** Object:  Database [Baseball]    Script Date: 4/13/2016 11:14:10 AM ******/
CREATE DATABASE [Baseball]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Baseball', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\Baseball.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Baseball_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\Baseball_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO

ALTER DATABASE [Baseball] SET COMPATIBILITY_LEVEL = 120
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Baseball].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [Baseball] SET ANSI_NULL_DEFAULT OFF 
GO

ALTER DATABASE [Baseball] SET ANSI_NULLS OFF 
GO

ALTER DATABASE [Baseball] SET ANSI_PADDING OFF 
GO

ALTER DATABASE [Baseball] SET ANSI_WARNINGS OFF 
GO

ALTER DATABASE [Baseball] SET ARITHABORT OFF 
GO

ALTER DATABASE [Baseball] SET AUTO_CLOSE OFF 
GO

ALTER DATABASE [Baseball] SET AUTO_SHRINK OFF 
GO

ALTER DATABASE [Baseball] SET AUTO_UPDATE_STATISTICS ON 
GO

ALTER DATABASE [Baseball] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO

ALTER DATABASE [Baseball] SET CURSOR_DEFAULT  GLOBAL 
GO

ALTER DATABASE [Baseball] SET CONCAT_NULL_YIELDS_NULL OFF 
GO

ALTER DATABASE [Baseball] SET NUMERIC_ROUNDABORT OFF 
GO

ALTER DATABASE [Baseball] SET QUOTED_IDENTIFIER OFF 
GO

ALTER DATABASE [Baseball] SET RECURSIVE_TRIGGERS OFF 
GO

ALTER DATABASE [Baseball] SET  DISABLE_BROKER 
GO

ALTER DATABASE [Baseball] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO

ALTER DATABASE [Baseball] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO

ALTER DATABASE [Baseball] SET TRUSTWORTHY OFF 
GO

ALTER DATABASE [Baseball] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO

ALTER DATABASE [Baseball] SET PARAMETERIZATION SIMPLE 
GO

ALTER DATABASE [Baseball] SET READ_COMMITTED_SNAPSHOT OFF 
GO

ALTER DATABASE [Baseball] SET HONOR_BROKER_PRIORITY OFF 
GO

ALTER DATABASE [Baseball] SET RECOVERY SIMPLE 
GO

ALTER DATABASE [Baseball] SET  MULTI_USER 
GO

ALTER DATABASE [Baseball] SET PAGE_VERIFY CHECKSUM  
GO

ALTER DATABASE [Baseball] SET DB_CHAINING OFF 
GO

ALTER DATABASE [Baseball] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO

ALTER DATABASE [Baseball] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO

ALTER DATABASE [Baseball] SET DELAYED_DURABILITY = DISABLED 
GO

ALTER DATABASE [Baseball] SET  READ_WRITE 
GO


USE [Baseball]
GO

/****** Object:  Table [dbo].[League]    Script Date: 4/13/2016 11:14:35 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[League](
	[LeagueID] [int] IDENTITY(1,1) NOT NULL,
	[LeagueName] [varchar](50) NULL,
 CONSTRAINT [PK_League] PRIMARY KEY CLUSTERED 
(
	[LeagueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


USE [Baseball]
GO

/****** Object:  Table [dbo].[Player]    Script Date: 4/13/2016 11:22:13 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Player](
	[PlayerID] [int] IDENTITY(1,1) NOT NULL,
	[TeamID] [int] NULL,
	[PlayerName] [varchar](50) NULL,
	[PlayerNumber] [int] NULL,
	[Position] [nchar](2) NULL,
	[BattingAvg] [decimal](18, 0) NULL,
	[YearsPlayed] [int] NULL,
 CONSTRAINT [PK_Player] PRIMARY KEY CLUSTERED 
(
	[PlayerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[Player]  WITH CHECK ADD  CONSTRAINT [FK_Player_Team] FOREIGN KEY([TeamID])
REFERENCES [dbo].[Team] ([TeamID])
GO

ALTER TABLE [dbo].[Player] CHECK CONSTRAINT [FK_Player_Team]
GO

USE [Baseball]
GO

/****** Object:  Table [dbo].[Team]    Script Date: 4/13/2016 11:22:25 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Team](
	[TeamID] [int] IDENTITY(1,1) NOT NULL,
	[LeagueID] [int] NULL,
	[TeamName] [varchar](50) NULL,
	[Manager] [varchar](50) NULL,
 CONSTRAINT [PK_Team] PRIMARY KEY CLUSTERED 
(
	[TeamID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[Team]  WITH CHECK ADD  CONSTRAINT [FK_Team_League] FOREIGN KEY([LeagueID])
REFERENCES [dbo].[League] ([LeagueID])
GO

ALTER TABLE [dbo].[Team] CHECK CONSTRAINT [FK_Team_League]
GO

insert into League (LeagueName)
Values ('National League')

insert into Team (LeagueID, TeamName, Manager)
Values (1, 'Braves','Fredi Gonz�lez'),(2, 'Cubs', 'Joe Madden'),(3, 'Dodgers', 'Dave Roberts')

insert into Player (TeamID, PlayerName, PlayerNumber, Position, BattingAvg, YearsPlayed)
values(1, 'A.J. Pierzynski', 15, 'C', .125, 21),
(1, 'Freddie Freeman', 5, '1B', .111, 7),
(1, 'Drew Stubbs', 2, 'CF', .250, 8),
(2, 'Miguel Montero', 47, 'C', .278, 11),
(2, 'Anthony Rizzo', 44, '1B', .240, 6),
(2, 'Jason Heyward', 22, 'RF', .250, 7),
(3, 'Adri�n Gonz�lez', 23, '1B', .419, 15),
(3, 'Chase Utley', 26, '2B', .310, 16),
(3, 'A.J. Ellis', 17, 'C', .133, 9)

USE [Baseball]
GO

/****** Object:  StoredProcedure [dbo].[addplayer]    Script Date: 4/13/2016 11:27:50 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[addplayer]
(
@TeamID int,
@PlayerName varchar(50),
@PlayerNumber int,
@Position varchar(50),
@BattingAvg decimal(18,0),
@YearsPlayed int
)AS

INSERT into Player
(
TeamID,
PlayerName,
PlayerNumber,
Position,
BattingAvg,
YearsPlayed
)
Values
(
@TeamID,
@PlayerName,
@PlayerNumber,
@Position,
@BattingAvg,
@YearsPlayed
)

GO


USE [Baseball]
GO

/****** Object:  StoredProcedure [dbo].[addteam]    Script Date: 4/13/2016 11:28:01 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[addteam]
(
@LeagueID int,
@TeamName varchar(50),
@Manager varchar(50)
)AS

INSERT into Team
(
LeagueID,
TeamName,
Manager
)
Values
(
@LeagueID,
@TeamName,
@Manager
)

GO


USE [Baseball]
GO

/****** Object:  StoredProcedure [dbo].[deleteplayer]    Script Date: 4/13/2016 11:30:46 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[deleteplayer]
(
@PlayerID int
)AS


DELETE FROM Player
WHERE PlayerID = @PlayerID;


GO


USE [Baseball]
GO

/****** Object:  StoredProcedure [dbo].[playersinteam]    Script Date: 4/13/2016 11:30:56 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[playersinteam]
(
@TeamID int
)AS

SELECT * FROM Player
Where TeamID = @TeamID;

GO


USE [Baseball]
GO

/****** Object:  StoredProcedure [dbo].[teamsinleague]    Script Date: 4/13/2016 11:31:38 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[teamsinleague]
(
@LeagueID int
)AS

SELECT * FROM League
LEFT JOIN Team
ON TEAM.LeagueID=League.LeagueID;

GO


USE [Baseball]
GO

/****** Object:  StoredProcedure [dbo].[tradeplayer]    Script Date: 4/13/2016 11:32:00 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[tradeplayer]
(
@PlayerID1 int,
@PlayerID2 int,
@TeamID1 int,
@TeamID2 int
)AS


Update Player
Set TeamID = @TeamID2
Where PlayerID = @PlayerID1

Update Player
Set TeamID = @TeamID1
Where PlayerID = @PlayerID2


GO






